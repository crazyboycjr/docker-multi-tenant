#!/bin/bash

if [ $UID -ne 0 ]; then
	echo "please execute this script as root"
	exit 1
fi

function create_docker_network() {
	DEV=$1
	NAME=$2

	docker network ls | grep $NAME 2>&1 >/dev/null
	if [ $? -eq 0 ]; then
		echo $NAME network has been created
		return 0
	fi

	echo "This may take several seconds, please wait..."

	SUBNET=`ip a show $DEV | grep 'inet ' | awk '{print $2}'`
	GW=`ip r | grep $DEV | grep via | awk '{print $3}'`

	echo $DEV, $NAME, $SUBNET, $GW

	docker network create -d sriov --subnet=$SUBNET -o netdevice=$DEV $NAME
	num_vfs=`cat /sys/class/net/$DEV/device/sriov_numvfs`
	for i in {0..$num_vfs}; do
		# set speed to 10Gbps
		sudo ip link set $DEV vf $i max_tx_rate 10000 min_tx_rate 10000
	done
}

IMAGE=byteps-mlt-test-libvma-fix

function create_container() {
	DEV=$1
	NET_NAME=$2
	CONTAINER_NAME=$3
	GPU_ID=$4
	GW=`ip r | grep $DEV | grep via | awk '{print $3}'`
	IP=`echo $GW | awk 'BEGIN{FS="."}{print $1 "." $2 "." $3 ".100"}'`

	echo "conatiner: $CONTAINER_NAME, IP: $IP"

	docker_rdma_sriov run -it -d --name=$CONTAINER_NAME --net=$NET_NAME --ip=$IP --runtime=nvidia -e NVIDIA_VISIBLE_DEVICES=$GPU_ID --ulimit memlock=-1 --shm-size=32768m --cap-add=IPC_LOCK --device=/dev/infiniband $IMAGE zsh
	pid=$(sudo docker inspect -f '{{.State.Pid}}' $CONTAINER_NAME)
	nsenter -t $pid -n ip route add 10.0.0.0/8 dev eth0 via $GW

	#mkdir -p /var/run/netns
	#ln -s /proc/$pid/ns/net /var/run/netns/$pid
	#ip netns exec $pid ip route add 10.0.0.0/8 dev eth0 via $GW

	#docker exec -it $CONTAINER_NAME ip route add 10.0.0.0/8 dev eth0 via $GW

	docker network connect bridge $CONTAINER_NAME

	# inspect
	docker exec -it $CONTAINER_NAME ip addr
	docker exec -it $CONTAINER_NAME ip route
}


create_docker_network rdma0 ns0
create_docker_network rdma2 ns2

create_container rdma0 ns0 mlt-tenant-0 0
create_container rdma2 ns2 mlt-tenant-1 1

# docker network create -d sriov --subnet=10.0.1.0/24 -o netdevice=rdma0 rdma0
