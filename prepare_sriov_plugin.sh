#!/bin/bash

if [ $UID -ne 0 ]; then
	echo "please execute this script as root"
	exit 1
fi

# docker pull rdma/sriov-plugin

# docker pull rdma/container_tools_installer

docker run --rm --net=host --name container_tools_installer -v /usr/bin:/tmp rdma/container_tools_installer

docker run -d --rm --name sriov-plugin -v /run/docker/plugins:/run/docker/plugins -v /etc/docker:/etc/docker -v /var/run:/var/run --net=host --privileged rdma/sriov-plugin
